package io.gitlab.atlexer;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;


public class MCOSpeedrun implements Listener{
		
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e) {
		if(!Commands.allowMovment) {
			e.setCancelled(true);
		}else {
			e.setCancelled(false);
		}
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		createBoard(e.getPlayer());
	}
	
	
	public void createBoard(Player player) {
		switch(Commands.step) {
		case 0:
			Commands.timeOverworld = Commands.time;
			break;
		case 1:
			Commands.timeNether = Commands.time;
			break;
		case 2:
			Commands.timeEnd = Commands.time;
			break;
		case 3:
			Commands.timeEnderDragon = Commands.time;
			break;
		case 4:
			Commands.status = "Time: "+ getTimeString(Commands.timeEnderDragon);
			break;
		default:
			Commands.status = "Time: "+ getTimeString(Commands.timeEnderDragon);
			break;
		}
		
		ScoreboardManager manager = Bukkit.getScoreboardManager();
		Scoreboard board = manager.getNewScoreboard();
		Objective obj = board.registerNewObjective("MCOSScoreboard", "dummy", ChatColor.translateAlternateColorCodes('&', "&a&l<< &2&l"+ Commands.status+" &a&l>>"));
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
				
		Score timeOverworld = 			obj.getScore("Start:........................"+ getTimeString(Commands.timeOverworld));
		timeOverworld.setScore(3);
		
		Score timeNether = 				obj.getScore("Nether:...................."+ getTimeString(Commands.timeNether));
		timeNether.setScore(2);
		
		Score timeEnd =					obj.getScore("End:............................"+ getTimeString(Commands.timeEnd));
		timeEnd.setScore(1);
		
		Score timeEnderDragon =			obj.getScore("Ender Dragon:.."+ getTimeString(Commands.timeEnderDragon));
		timeEnderDragon.setScore(0);
		
		player.setScoreboard(board);		
	}
	
	public void updateBoards() {
		if(!Bukkit.getOnlinePlayers().isEmpty()) {
			for(Player online : Bukkit.getOnlinePlayers()) {
				createBoard(online);
			}
		}
	}
	
	
	public void ActionBarMessage(Player player, String arg) {
		player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(arg));
	}
	
	private String getTimeString(final int time) {
        final int hours = time / 3600;
        final int minutes = time % 3600 / 60;
        final int seconds = time % 60;
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

}
