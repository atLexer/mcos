package io.gitlab.atlexer;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;





public class Main extends JavaPlugin{
	
	MCOSpeedrun mcos = new MCOSpeedrun();
	
	private static Main instance;	
	public static Main getInstance() {
		return instance;
	}
	

	
//Overrides
	@Override
	public void onEnable() {
		instance = this;
		getServer().getPluginManager().registerEvents(mcos, this);
		

	//Commands
		this.getCommand("loc").setExecutor(new Commands(this));
		this.getCommand("del").setExecutor(new Commands(this));
		this.getCommand("locs").setExecutor(new Commands(this));
		this.getCommand("start").setExecutor(new Commands(this));
		this.getCommand("pause").setExecutor(new Commands(this));
		this.getCommand("reset").setExecutor(new Commands(this));
		this.getCommand("next").setExecutor(new Commands(this));
		
	//Creatboards on reload
		if(!Bukkit.getOnlinePlayers().isEmpty()) {
			for(Player online : Bukkit.getOnlinePlayers()) {
				mcos.createBoard(online);
			}
		}
	}
	
	@Override
	public void onDisable() {
		instance = null;
		//shutdown, reloads, plugin reloads
	}
	
}