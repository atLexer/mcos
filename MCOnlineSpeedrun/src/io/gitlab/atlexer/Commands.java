package io.gitlab.atlexer;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scheduler.BukkitRunnable;



public class Commands implements CommandExecutor{
	
	private Main plugin;
	MCOSpeedrun mcos = new MCOSpeedrun();
	private BukkitTask timer;
	public static int step;
	public static int time;
	public static int timeOverworld;
	public static int timeNether;
	public static int timeEnd;
	public static int timeEnderDragon;
	private static boolean timerOn = false;
	
	public static String status = "Write: /start";
	
	private static ArrayList<String[]> LocationsList = new ArrayList<String[]>();
	public static boolean allowMovment;
	
	public Commands(final Main plugin) {
		Commands.step = 0;
		Commands.time = 0;
		Commands.timeOverworld = 0;
		Commands.timeNether = 0;
		Commands.timeEnd = 0;
		Commands.timeEnderDragon = 0;
		this.plugin = plugin;
    }
	
public static void setAllowMovment(boolean b) {
	Commands.allowMovment = b;
}

private void startTimer() {	
	this.timer = Bukkit.getScheduler().runTaskTimer((Plugin)this.plugin, () -> {
		if(Commands.allowMovment) {
			Commands.time++;
			mcos.updateBoards();
		}
		
		return;
	}, 0L, 20L);
	
			
	
}

public void countdown() {
	String arg;
	if(Commands.timerOn) {
		arg = "continues";
	}else{
		arg = "starts";
	}
	if(!Bukkit.getOnlinePlayers().isEmpty()) {
		for(Player online : Bukkit.getOnlinePlayers()) {
			mcos.ActionBarMessage(online, ("Run "+arg+" in 3.."));
			//online.sendMessage("3");
			
			new BukkitRunnable() {
				int i = 3;
		        @Override
		        public void run() {
		        	i--;
		        	if(i>0) {
		        		mcos.ActionBarMessage(online, (i+".."));
			        	//online.sendMessage(""+i);
		        	}else {
		        		mcos.ActionBarMessage(online, ("GO!"));
			        	//online.sendMessage("GO!");
			        	cancel();
		        	}   	
		        }      
		    }.runTaskTimer(this.plugin, 20, 20);
		    			
			
		}
	}
	
    
    
}

private void resetTimer() {
    setAllowMovment(false);
    Commands.step = 0;
    Commands.time = 0;
    Commands.timeOverworld = 0;
	Commands.timeNether = 0;
	Commands.timeEnd = 0;
	Commands.timeEnderDragon = 0;
    mcos.updateBoards();
}

private void stopTimer() {
	this.timer.cancel();
}
	
	
public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
	
	//loc
		if(label.equals("loc")) {
			if(sender instanceof Player) {
				//player
				Player player = (Player) sender;
				if(args.length != 0) {
					Location loc = player.getLocation();
					String locString = args[0] +": X:"+(int)loc.getX()+", Y:"+(int)loc.getY()+", Z:"+(int)loc.getZ();
					if(loc.getWorld().getName().equals("world")) {
						locString = ChatColor.GREEN + "" + locString;
					}else if(loc.getWorld().getName().equals("world_nether")) {
						locString = ChatColor.RED + "" + locString;
					}
					LocationsList.add(new String[]{args[0],locString});
					player.sendMessage(locString);
					return true;
				}else {
					player.sendMessage("Usage: /loc <location name>");
				}
			}else {
			//console
			return true;
			}
		}
		
//del	
		if(label.equals("del")) {
			if(sender instanceof Player) {
				Player player = (Player) sender;
				if(args.length != 0) {
					//player
					for(int i = 0;i < LocationsList.size();i++) {
						if(LocationsList.get(i)[0].equals(args[0])) {
							player.sendMessage("Removed: "+LocationsList.get(i)[0]);
							LocationsList.remove(i);
						}
					}
					return true;
				}else {
					player.sendMessage("Usage: /del <location name> | find all locations with /locs");
				}
			}else {
				//console
				return true;
			}
			
		}
	
//locs
		if(label.equals("locs")) {
			if(sender instanceof Player) {
				//player
				Player player = (Player) sender;
				for(int i = 0;i < LocationsList.size();i++) {
					player.sendMessage(LocationsList.get(i)[1]);
				}
				return true;
			}else {
				//console
				return true;
			}
		}

//start
		if(label.equals("start")) {
			if(sender instanceof Player) {
				Player player = (Player) sender;
				if(!Commands.timerOn) {		
					//send action message
					countdown();
					new BukkitRunnable() {
				        
			            @Override
			            public void run() {
			                // What you want to schedule goes here
			            	startTimer();
			            	Commands.status = "MCOSpeedrun";
			            	setAllowMovment(true);
			            }
			            
			        }.runTaskLater(this.plugin, 60);
			        Commands.timerOn = true;
					return true;
				}else {
					if(!allowMovment) {
						countdown();
						new BukkitRunnable() {
					        
				            @Override
				            public void run() {
				                // What you want to schedule goes here
				            	Commands.status = "MCOSpeedrun";
				            	setAllowMovment(!allowMovment);
				            }
				            
				        }.runTaskLater(this.plugin, 60);
						return true;
					}else {
						player.sendMessage("Timer already running. Use /pause to pause the run.");
						return true;
					}
				}
				
			}else {
				//console
				return true;
			}
		}
		
//pause
		if(label.equals("pause")) {
			if(sender instanceof Player) {
				if(!Commands.timerOn) {
					Player player = (Player) sender;
					player.sendMessage("Timer not running. Use /start to start the run.");
					return true;
				}
				if(!allowMovment) {
					countdown();
					new BukkitRunnable() {
				        
			            @Override
			            public void run() {
			                // What you want to schedule goes here
			            	Commands.status = "MCOSpeedrun";
			            	setAllowMovment(!allowMovment);
			            }
			            
			        }.runTaskLater(this.plugin, 60);
					return true;
				}else {
					if(Commands.step < 4) {
					Commands.status = "Paused..";
					mcos.updateBoards();
					setAllowMovment(!allowMovment);
					}
					return true;
				}
				
			}else {
				//console
				return true;
			}
		}
//reset
		
		if(label.equals("reset")) {
			if(sender instanceof Player) {
				//player
				Player player = (Player) sender;		
				mcos.ActionBarMessage(player, ("Reset Timer!"));
				Commands.status = "Write: /start";
				resetTimer();
				return true;
			}else {
				//console
				stopTimer();
				return true;
			}
		}
		
//nextStep

		if(label.equals("next")) {
			if(sender instanceof Player) {
				nextStep();
				return true;
			}else {
				//console
				nextStep();
				return true;
			}
		}
	
	
		
		return false;	
	}

public static void nextStep() {
	Commands.step++;
}


}
